# minesweeper-API - Solution

Decisions taken and important notes.

## Public repository

https://bitbucket.org/asommariva/minesweeper-api/

## What to build

### Design and implement a documented RESTful API for the game

#### API Spec

I've decided to use Swagger for the API specification and code generation.
The `swagger.json` as been placed in its own module called `minesweeper-api-spec` and imported as a dependency in the implementation module.
The reasons to keep it a separate resource are:
- ease of development, build setup for this module is actually simpler
- future growth, in the future this could become an independent artifact shared across API and UI applications

#### API Impl

I've chosen Sprint Boot for the API implementation with MongoDB.
MongoDB is running in memory since I wanted to avoid external dependencies as much as possible. 
In order to achieve this, the following dependencies have been added:

```
<dependency>
  <groupId>de.flapdoodle.embed</groupId>
  <artifactId>de.flapdoodle.embed.mongo</artifactId>
  <version>1.50.0</version>
</dependency>
<dependency>
  <groupId>cz.jirutka.spring</groupId>
  <artifactId>embedmongo-spring</artifactId>
  <version>1.3.1</version>
</dependency>
```

And, the following [configuration file](minesweeper-api-impl/src/main/java/com/deviget/minesweeper/config/db/MongoDBConfiguration.java) to download, start and stop the database.

**Note:** In-memory MongoDB configuration should be changed for production usage.

#### API Documentation and Client

I've decided to publish the [API Spec in SwaggerHub](https://app.swaggerhub.com/apis/andres-sommariva/Minesweeper-API/1.0.0) which provides both, a nice template for the API documentation and the ability to execute requests.

#### Application Deployment

I've decided to host the [application in Heroku](https://as-minesweeper-api.herokuapp.com).

**Note:** Although I tried to make Bitbucket Pipelines work to automatically deploy to Heroku, it kept failing (reason is not clear for me) so I decided to deploy manually from my local development environment.

### When a cell with no adjacent mines is revealed, all adjacent squares will be revealed (and repeat)

This is achieved through a recursive function in the `Board` object.

### Ability to 'flag' a cell with a question mark or red flag

It is possible to place a red flag on a cell. If you try to uncover a cell with a red flag no action will take place.

### Detect when game is over

When a mine is hit, the game `status` will change to `GAMEOVER`. 
If you try to make a play in a finalized game an HTTP error code 409 will be returned.

### Persistence

As mentioned before, I've decided to use MongoDB to store the whole `game` object.

## Known limitations

- `Time tracking` feature is not implemented yet.
- `Ability to start a new game and preserve/resume the old ones` feature is not implemented yet.
- `Ability to select the game parameters: number of rows, columns, and mines` is implemented through `complexity` when creating a game, so rows/columns/mines are predefined.
- `Ability to support multiple users/accounts` feature is not implemented yet.
- First cell hit could be a mine. 