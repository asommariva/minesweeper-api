# minesweeper-API - How To Guides

How to run, test and play the game.

## How To

## Run

```
cd minesweeper-api-impl
mvn sprint-boot:run
```

## Test

```
mvn test
```

## Play

### 1. Login

First step is login to the application. You'll need to login to start a game and play.

Available users and their passwords:

- player1 / player1
- player2 / player2
- player3 / player3

In order to login, you'll use the `/login` endpoint:

```
POST /login

Body:
{
    "username": "player1",
    "password": "player1"
}
```

As a response, you'll get the JWT to use in future calls.

### 2. Start a game

In order to setup create a new game, you'll use the `/games` endpoint:

```
POST /games

Body:
{
    "player": "player1",
    "complexity": "low"
}
```

As a response, you'll get the `game id` of the game just created.

### 3. Read the game

Once a game is created you can read the details using the `/games` endpoint using the `id` provided during creation:

```
GET /games/{id}
```

### 4. Uncover a cell
The basic action you can perform is to `uncover` a cell.

```
POST /games/{id}/plays

{
    "type": "uncover",
    "row": 0,
    "col": 3
}
```

If the cell is not a mine, adjacent cells will be uncovered. 
In case the cell is a mine, the game status will change to GAMEOVER.

### 5. Mark a cell with a red flag

You can mark a cell with a `red flag`.

```
POST /games/{id}/plays

{
    "type": "red_flag",
    "row": 0,
    "col": 3
}
```

If a cell is marked with a red flag, you will not be able to uncover it.
The API will not report an error, it will simply have no effect in the cell status.