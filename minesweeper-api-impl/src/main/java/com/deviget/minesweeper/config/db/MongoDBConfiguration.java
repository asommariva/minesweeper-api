package com.deviget.minesweeper.config.db;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.net.UnknownHostException;

@Configuration
@EnableMongoRepositories(basePackages = {"com.deviget.minesweeper.*.repository"})
public class MongoDBConfiguration extends AbstractMongoClientConfiguration {

    private static final String MONGO_DB_URL = "localhost";
    private static final int MONGO_DB_PORT = 27017;

    MongodStarter starter = MongodStarter.getDefaultInstance();
    MongodExecutable mongodExecutable;

    /*
     * Use the standard Mongo driver API to create a com.mongodb.client.MongoClient instance.
     */
    public @Bean MongoClient mongoClient() {
        //return MongoClients.create(String.format("mongodb://%s:%d", MONGO_DB_URL, MONGO_DB_PORT));
        return MongoClients.create("mongodb://localhost:27017");
    }

    @PostConstruct
    public void construct() throws UnknownHostException, IOException {
        IMongodConfig mongodConfig = new MongodConfigBuilder().version(Version.Main.PRODUCTION).net(new Net(MONGO_DB_URL, MONGO_DB_PORT, true)).build();
        mongodExecutable = starter.prepare(mongodConfig);
        MongodProcess mongod = mongodExecutable.start();
    }

    @PreDestroy
    public void destroy() {
        if (mongodExecutable != null) {
            mongodExecutable.stop();
        }
    }

    @Override
    protected String getDatabaseName() {
        return "minesweeper";
    }

}
