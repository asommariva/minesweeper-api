package com.deviget.minesweeper.config.security;

public class SecurityConstants {
    public static final String AUTH_LOGIN_URL = "/login";

    public static final String JWT_SECRET = "shouldnotbehardcoded_shouldnotbehardcoded_shouldnotbehardcoded_shouldnotbehardcoded";

    // JWT token defaults
    public static final String TOKEN_HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String TOKEN_TYPE = "JWT";
    public static final String TOKEN_ISSUER = "secure-api";
    public static final String TOKEN_AUDIENCE = "secure-app";
}
