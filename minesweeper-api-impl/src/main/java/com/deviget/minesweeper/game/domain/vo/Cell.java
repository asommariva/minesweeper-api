package com.deviget.minesweeper.game.domain.vo;

import com.deviget.minesweeper.dto.CellStatus;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
public class Cell {
    @Setter
    private Boolean isMine;
    @Setter
    private CellStatus status;
    private Integer adjacentMineCount;

    @Builder
    public Cell(Boolean isMine) {
        this.isMine = isMine;
        this.status = CellStatus.COVERED;
        this.adjacentMineCount = 0;
    }

    public void adjacentMineAdded() {
        this.adjacentMineCount++;
    }
}
