package com.deviget.minesweeper.game.domain.vo;

import com.deviget.minesweeper.dto.CellStatus;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Random;

@Slf4j
@Getter
public class Board {
    private BoardConfig config;
    private ArrayList<ArrayList<Cell>> cells;

    @Builder
    public Board(BoardConfig config) {
        this.config = config;
        this.cells = new ArrayList<ArrayList<Cell>>();
        initializeBoard();
    }

    /**
     * Uncover cells.
     * @param row
     * @param col
     * @return 'true' indicates a mine was hit, 'false' otherwise.
     */
    public boolean uncover(int row, int col) {
        Cell cell = this.cells.get(row).get(col);

        if (CellStatus.RED_FLAG.equals(cell.getStatus())) {
            return false;
        }

        if (cell.getIsMine()) {
            cell.setStatus(CellStatus.UNCOVERED);
            return true;
        }

        processAdjacentCells(row, col);
        return false;
    }

    public void redFlag(int row, int col) {
        Cell cell = this.cells.get(row).get(col);
        cell.setStatus(CellStatus.RED_FLAG);
    }

    /**
     * Check if there are available cells to uncover.
     * @return 'true' indicates there are cells to uncover, 'false' otherwise.
     */
    public boolean hasUncoveredCells() {
        int uncoveredCellsCount = this.cells.stream()
                .map(row -> ((int) row.stream()
                        .filter(cell -> CellStatus.UNCOVERED.equals(cell.getStatus()))
                        .count()))
                .reduce(0, Integer::sum);
        return ((uncoveredCellsCount + this.config.getTotalMines()) != (this.config.getCols() * this.config.getRows()));
    }

    /**
     * Initialize the board when a new game is created.
     */
    private void initializeBoard() {
        initializeCells();
        placeMines();
    }

    /**
     * Cells will be empty and covered.
     */
    private void initializeCells() {
        for (int row = 0; row < config.getRows(); row++) {
            cells.add(row, new ArrayList<>());
            for (int col = 0; col < config.getCols(); col++) {
                cells.get(row).add(col, Cell.builder().isMine(false).build());
            }
        }
    }

    /**
     * Place mines according to config, and calculate adjacent mines count.
     */
    private void placeMines() {
        Random random = new Random();
        int minesLeft = config.getTotalMines();
        while (minesLeft > 0) {
            int row = random.nextInt(config.getRows());
            int col = random.nextInt(config.getCols());

            if (cells.get(row).get(col).getIsMine())
                continue;

            cells.get(row).get(col).setIsMine(true);
            calculateAdjacentMineCount(row, col);
            minesLeft--;
        }
    }

    /**
     * Calculate adjacent mines count for a given cell.
     * @param row
     * @param col
     */
    private void calculateAdjacentMineCount(int row, int col) {
        for (int dx = -1; dx <= 1; dx++) {
            for (int dy = -1; dy <= 1; dy++) {
                if (
                        (dx != 0 || dy != 0) &&
                        (row + dy) >= 0 && (row + dy) < config.getRows() &&
                        (col + dx) >= 0 && (col + dx) < config.getCols()
                ) {
                    cells.get(row + dy).get(col + dx).adjacentMineAdded();
                }
            }
        }
    }

    /**
     * Recursively process adjacent cells when one is uncovered to uncover others.
     * @param row
     * @param col
     */
    private void processAdjacentCells(int row, int col) {
        Cell cell = this.cells.get(row).get(col);

        if (cell.getIsMine() || CellStatus.UNCOVERED.equals(cell.getStatus())) {
            return;
        }

        cell.setStatus(CellStatus.UNCOVERED);

        if (cell.getAdjacentMineCount() > 0) {
            return;
        }

        for (int dx = -1; dx <= 1; dx++) {
            for (int dy = -1; dy <= 1; dy++) {
                if (
                        (dx != 0 || dy != 0) &&
                                (row + dy) >= 0 && (row + dy) < this.config.getRows() &&
                                (col + dx) >= 0 && (col + dx) < this.config.getCols()
                ) {
                    processAdjacentCells(row + dy, col + dx);
                }
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Current board layout:");
        for (int y=0; y < config.getRows(); y++) {
            builder.append("\n");
            for (int x=0; x < config.getCols(); x++) {
                Cell cell = cells.get(y).get(x);
                builder.append(
                    String.format(" %s",
                        cell.getIsMine() ? "M" :
                            CellStatus.UNCOVERED.equals(cell.getStatus())? "X" :
                                    cell.getAdjacentMineCount() > 0 ?cell.getAdjacentMineCount()  : "-"
                    ));
            }
        }
        return builder.toString();
    }
}
