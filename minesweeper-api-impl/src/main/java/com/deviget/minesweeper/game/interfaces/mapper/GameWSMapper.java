package com.deviget.minesweeper.game.interfaces.mapper;

import com.deviget.minesweeper.dto.CellWS;
import com.deviget.minesweeper.dto.GameWS;
import com.deviget.minesweeper.game.application.dto.CellDto;
import com.deviget.minesweeper.game.application.dto.GameDto;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

@Component
public class GameWSMapper {

    private MapperFacade facade;

    @PostConstruct
    public void init() {
        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        mapperFactory.classMap(CellDto.class, CellWS.class)
                .byDefault()
                .register();
        mapperFactory.classMap(GameDto.class, GameWS.class)
                .exclude("createdOn")
                .byDefault()
                .customize(new CustomMapper<GameDto, GameWS>() {
                    @Override
                    public void mapAtoB(GameDto gameDto, GameWS gameWS, MappingContext context) {
                        // createdOn
                        gameWS.setCreatedOn(gameDto.getCreatedOn().toInstant().atOffset(ZoneOffset.UTC));
                        // cells
                        List<List<CellDto>> cells = gameDto.getCells();
                        List<List<CellWS>> cellsWS = new ArrayList<>();
                        for (int row=0; row < cells.size(); row++) {
                            cellsWS.add(new ArrayList<CellWS>());
                            for (int col=0; col < cells.get(row).size(); col++) {
                                CellWS cellWS = facade.map(cells.get(row).get(col), CellWS.class);
                                cellWS.setY(row);
                                cellWS.setX(col);
                                cellsWS.get(row).add(cellWS);
                            }
                        }
                        gameWS.setCells(cellsWS);
                    }
                })
                .register();
        facade = mapperFactory.getMapperFacade();
    }

    public GameWS toGameWS(GameDto gameDto) {
        return facade.map(gameDto, GameWS.class);
    }

}
