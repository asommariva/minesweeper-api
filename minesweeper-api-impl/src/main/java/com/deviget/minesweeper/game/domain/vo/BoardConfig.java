package com.deviget.minesweeper.game.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Builder
@AllArgsConstructor
@Getter
public class BoardConfig {
    private Integer rows;
    private Integer cols;
    private Integer totalMines;
}
