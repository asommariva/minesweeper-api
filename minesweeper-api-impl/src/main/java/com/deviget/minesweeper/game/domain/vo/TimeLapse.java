package com.deviget.minesweeper.game.domain.vo;

import java.util.Date;


public class TimeLapse {
    private Date start = null;
    private Date end = null;

    /**
     * Start time lapse tracking.
     */
    public void start() {
        if (this.start == null)
            this.start = new Date();
    }

    /**
     * End time lapse tracking.
     */
    public void end() {
        if (this.end == null)
            this.end = new Date();
    }

    /**
     * Get time spent in time lapse in seconds.
     * @return
     */
    public Long timeSpent() {
        Date compareTo = end;

        if (start == null)
            return 0L;

        if (end == null) {
            compareTo = new Date();
        }

        return (compareTo.getTime() - start.getTime()) / 1000;
    }
}
