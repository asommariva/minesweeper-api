package com.deviget.minesweeper.game.interfaces;

import com.deviget.minesweeper.api.GamesApi;
import com.deviget.minesweeper.dto.CreateGameRequest;
import com.deviget.minesweeper.dto.GameWS;
import com.deviget.minesweeper.dto.PlayGameRequest;
import com.deviget.minesweeper.game.application.GamesService;
import com.deviget.minesweeper.game.application.dto.GameDto;
import com.deviget.minesweeper.game.domain.exception.GameFinalizedException;
import com.deviget.minesweeper.game.interfaces.mapper.GameWSMapper;
import com.deviget.minesweeper.game.repository.exception.GameNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@Slf4j
@RestController
public class GamesApiImpl implements GamesApi {

    @Autowired
    private GamesService gamesService;

    @Autowired
    private GameWSMapper gameWSMapper;

    @Override
    public ResponseEntity<Object> createGame(@Valid CreateGameRequest body) {
        log.debug("Create a new game: {}", body.toString());
        canCreateGame(body);

        String gameId = gamesService.createGame(body.getPlayer(), body.getComplexity());

        JSONObject jsonResponse = new JSONObject();
        jsonResponse.put("id", gameId);
        return new ResponseEntity<>(jsonResponse.toString(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<GameWS> get(String id) {
        log.debug("Read game by id: {}", id);
        GameDto gameDto;

        try {
            gameDto = gamesService.readGame(id);
        } catch (GameNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Game not found", e);
        }

        return ResponseEntity.ok().body(gameWSMapper.toGameWS(gameDto));
    }

    @Override
    public ResponseEntity<GameWS> playGame(String id, @Valid PlayGameRequest body) {
        log.debug("Play game with id: {}", id);
        GameDto gameDto;

        try {
            gameDto = gamesService.playGame(id, body.getType(), body.getRow(), body.getCol());
        } catch (GameNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Game not found", e);
        } catch (GameFinalizedException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Can't play a finalized game", e);
        }

        return ResponseEntity.ok().body(gameWSMapper.toGameWS(gameDto));
    }

    private void canCreateGame(CreateGameRequest body) {
        UserDetails user = getAuthenticatedUser();
        if (!body.getPlayer().equals(user.getUsername())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You can't create games on behalf of another player.");
        }
    }

    private UserDetails getAuthenticatedUser() {
        return (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
