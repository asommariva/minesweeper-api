package com.deviget.minesweeper.game.application.dto;

import com.deviget.minesweeper.dto.Complexity;
import com.deviget.minesweeper.dto.GameStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GameDto {

    private String id;
    private String createdBy;
    private Date createdOn;
    private GameStatus status;
    private Complexity complexity;
    private BoardConfigDto boardConfig;
    private List<List<CellDto>> cells;

}
