package com.deviget.minesweeper.game.application.mapper;

import com.deviget.minesweeper.dto.CellStatus;
import com.deviget.minesweeper.game.application.dto.CellDto;
import com.deviget.minesweeper.game.domain.vo.Cell;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class CellDtoMapper {

    private MapperFacade facade;

    @PostConstruct
    public void init() {
        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        mapperFactory.classMap(Cell.class, CellDto.class)
                .customize(new CustomMapper<Cell, CellDto>() {
                    @Override
                    public void mapAtoB(Cell cell, CellDto cellDto, MappingContext context) {
                        cellDto.setStatus(cell.getStatus());
                        if (CellStatus.UNCOVERED.equals(cell.getStatus())) {
                            cellDto.setIsMine(cell.getIsMine());
                            cellDto.setAdjacentMineCount(cell.getAdjacentMineCount());
                        }
                    }
                })
                .register();
        facade = mapperFactory.getMapperFacade();
    }

    public CellDto toCellDto(Cell cell) {
        return facade.map(cell, CellDto.class);
    }
}
