package com.deviget.minesweeper.game.application;

import com.deviget.minesweeper.dto.Complexity;
import com.deviget.minesweeper.dto.PlayType;
import com.deviget.minesweeper.game.application.dto.GameDto;
import com.deviget.minesweeper.game.domain.exception.GameFinalizedException;
import com.deviget.minesweeper.game.repository.exception.GameNotFoundException;

public interface GamesService {

    /**
     * Create and start a new game.
     * @param player Author and owner.
     * @param complexity Level of complexity will determine, board size and amount of mines.
     * @return Game Id.
     */
    String createGame(String player, Complexity complexity);

    /**
     * Read a game by id.
     * @param gameId Game Id.
     * @return
     */
    GameDto readGame(String gameId) throws GameNotFoundException;

    /**
     *
     * @param gameId
     * @param playType
     * @param row
     * @param col
     * @return
     * @throws GameNotFoundException
     * @throws GameFinalizedException
     */
    GameDto playGame(String gameId, PlayType playType, int row, int col) throws GameNotFoundException, GameFinalizedException;

}
