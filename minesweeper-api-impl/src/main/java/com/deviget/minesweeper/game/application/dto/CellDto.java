package com.deviget.minesweeper.game.application.dto;

import com.deviget.minesweeper.dto.CellStatus;
import lombok.Data;

@Data
public class CellDto {
    private Boolean isMine;
    private CellStatus status;
    private Integer adjacentMineCount;
}
