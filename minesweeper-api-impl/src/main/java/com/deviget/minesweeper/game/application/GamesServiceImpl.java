package com.deviget.minesweeper.game.application;

import com.deviget.minesweeper.dto.Complexity;
import com.deviget.minesweeper.dto.PlayType;
import com.deviget.minesweeper.game.application.dto.GameDto;
import com.deviget.minesweeper.game.application.mapper.GameDtoMapper;
import com.deviget.minesweeper.game.domain.Game;
import com.deviget.minesweeper.game.domain.exception.GameFinalizedException;
import com.deviget.minesweeper.game.repository.GameRepository;
import com.deviget.minesweeper.game.repository.exception.GameNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GamesServiceImpl implements GamesService {

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private GameDtoMapper gameDtoMapper;

    @Override
    public String createGame(String player, Complexity complexity) {
        Game game = Game.builder()
                .createdBy(player)
                .complexity(complexity)
                .build();

        String gameId = gameRepository.save(game).getId();

        return gameId;
    }

    @Override
    public GameDto readGame(String gameId) throws GameNotFoundException {
        Game game = gameRepository.findById(gameId).orElseThrow(GameNotFoundException::new);

        GameDto gameDto = gameDtoMapper.toGameDto(game);
        return gameDto;
    }

    @Override
    public GameDto playGame(String gameId, PlayType playType, int row, int col) throws GameNotFoundException, GameFinalizedException {
        Game game = gameRepository.findById(gameId).orElseThrow(GameNotFoundException::new);

        switch (playType) {
            case UNCOVER:
                game.uncover(row, col);
                break;
            case RED_FLAG:
                game.redFlag(row, col);
                break;
        }
        game = gameRepository.save(game);

        GameDto gameDto = gameDtoMapper.toGameDto(game);
        return gameDto;
    }
}
