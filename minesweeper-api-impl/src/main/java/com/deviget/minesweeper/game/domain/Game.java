package com.deviget.minesweeper.game.domain;

import com.deviget.minesweeper.dto.Complexity;
import com.deviget.minesweeper.dto.GameStatus;
import com.deviget.minesweeper.game.domain.exception.GameFinalizedException;
import com.deviget.minesweeper.game.domain.vo.Board;
import com.deviget.minesweeper.game.domain.vo.BoardConfig;
import lombok.Builder;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Getter
@Document("Game")
public class Game {

    private final static Map<Complexity, BoardConfig> availableConfigs = new HashMap() {{
        put(Complexity.LOW, BoardConfig.builder().cols(9).rows(9).totalMines(10).build());
        put(Complexity.MEDIUM, BoardConfig.builder().cols(16).rows(16).totalMines(40).build());
        put(Complexity.HIGH, BoardConfig.builder().cols(16).rows(30).totalMines(99).build());
    }};

    @Id
    private String id;
    private String createdBy;
    private Date createdOn;
    private GameStatus status;
    private Complexity complexity;
    private Board board;

    @Builder
    public Game(String createdBy, Complexity complexity) {
        this.createdBy = createdBy;
        this.createdOn = new Date();
        this.complexity = complexity;
        this.status = GameStatus.INPROGRESS;
        this.board = Board.builder()
                .config(availableConfigs.get(complexity))
                .build();
    }

    /**
     * Uncover a cell and adjacent non-mine ones.
     * @param row
     * @param col
     * @throws GameFinalizedException if game status is GAMEOVER or GAMEWON
     */
    public void uncover(int row, int col) throws GameFinalizedException {
        checkCanPlayGame();

        // If a mine is hit, game is lost
        if (this.board.uncover(row, col)) {
            this.status = GameStatus.GAMEOVER;
        }

        // If there are no more cells to uncover, game is won
        if (!this.board.hasUncoveredCells()) {
            this.status = GameStatus.GAMEWON;
        }
    }

    /**
     * Mark a cell with a red flag
     * @param row
     * @param col
     * @throws GameFinalizedException if game status is GAMEOVER or GAMEWON
     */
    public void redFlag(int row, int col) throws GameFinalizedException {
        checkCanPlayGame();

        this.board.redFlag(row, col);
    }

    /**
     * throws GameFinalizedException if game status is GAMEOVER or GAMEWON
     */
    private void checkCanPlayGame() {
        if (GameStatus.GAMEOVER.equals(status) || GameStatus.GAMEWON.equals(status)) {
            throw new GameFinalizedException();
        }
    }
}
