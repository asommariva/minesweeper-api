package com.deviget.minesweeper.game.application.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Builder
@AllArgsConstructor
@Getter
public class BoardConfigDto {
    private Integer rows;
    private Integer cols;
    private Integer totalMines;
}
