package com.deviget.minesweeper.game.application.mapper;

import com.deviget.minesweeper.game.application.dto.CellDto;
import com.deviget.minesweeper.game.application.dto.GameDto;
import com.deviget.minesweeper.game.domain.Game;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class GameDtoMapper {

    private MapperFacade facade;

    @Autowired
    private CellDtoMapper cellDtoMapper;

    @PostConstruct
    public void init() {
        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        mapperFactory.classMap(Game.class, GameDto.class)
                .field("board.config", "boardConfig")
                .byDefault()
                .customize(new CustomMapper<Game, GameDto>() {
                    @Override
                    public void mapAtoB(Game game, GameDto gameDto, MappingContext context) {
                        List<List<CellDto>> cells = game.getBoard().getCells().stream().map(
                                cols -> cols.stream().map(cell -> cellDtoMapper.toCellDto(cell)).collect(Collectors.toList())
                        ).collect(Collectors.toList());
                        gameDto.setCells(cells);
                    }
                })
                .register();
        facade = mapperFactory.getMapperFacade();
    }

    public GameDto toGameDto(Game game) {
        return facade.map(game, GameDto.class);
    }

}
