package com.deviget.minesweeper.game.domain;

import com.deviget.minesweeper.dto.CellStatus;
import com.deviget.minesweeper.dto.Complexity;
import com.deviget.minesweeper.dto.GameStatus;
import com.deviget.minesweeper.game.domain.vo.Cell;
import lombok.Builder;
import lombok.Getter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GameUTest {

    @ParameterizedTest
    @EnumSource(Complexity.class)
    void test_gameInitialization(Complexity complexity)  {
        // given
        // parameterized complexity

        // when
        Game game = Game.builder()
                .createdBy("playerId")
                .complexity(complexity)
                .build();

        // then
        int minesCount = game.getBoard().getCells().stream()
                .map(row -> ((int) row.stream().filter(Cell::getIsMine).count()))
                .reduce(0, Integer::sum);
        assertEquals(game.getBoard().getConfig().getRows(), game.getBoard().getCells().size());
        assertEquals(game.getBoard().getConfig().getCols(), game.getBoard().getCells().get(0).size());
        assertEquals(game.getBoard().getConfig().getTotalMines(), minesCount);
    }

    @Test
    void test_uncover_emptyCell() {
        // given
        Game game = Game.builder()
                .createdBy("playerId")
                .complexity(Complexity.LOW)
                .build();

        // when
        Position emptyCell = findEmptyCell(game);
        game.uncover(emptyCell.getRow(), emptyCell.getCol());

        // then
        assertEquals(GameStatus.INPROGRESS, game.getStatus());
    }

    @Test
    void test_uncover_mineCell() {
        // given
        Game game = Game.builder()
                .createdBy("playerId")
                .complexity(Complexity.LOW)
                .build();

        // when
        Position mineCell = findMineCell(game);
        game.uncover(mineCell.getRow(), mineCell.getCol());

        // then
        assertEquals(GameStatus.GAMEOVER, game.getStatus());
        assertEquals(CellStatus.UNCOVERED, game.getBoard().getCells().get(mineCell.getRow()).get(mineCell.getCol()).getStatus());
        assertEquals(true, game.getBoard().getCells().get(mineCell.getRow()).get(mineCell.getCol()).getIsMine());
    }

    @Builder
    @Getter
    private static class Position {
        private int row;
        private int col;
    }

    private Position findEmptyCell(Game game) {
        return findCell(game, false);
    }

    private Position findMineCell(Game game) {
        return findCell(game, true);
    }

    private Position findCell(Game game, boolean isMine) {
        for (int y = 0; y < game.getBoard().getConfig().getRows(); y++) {
            for (int x = 0; x < game.getBoard().getConfig().getCols(); x++) {
                Cell cell = game.getBoard().getCells().get(y).get(x);
                if (cell.getIsMine() == isMine) {
                    return Position.builder()
                            .row(y)
                            .col(x)
                            .build();
                }
            }
        }
        return null;
    }

}
