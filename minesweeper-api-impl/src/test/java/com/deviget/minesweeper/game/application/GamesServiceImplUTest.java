package com.deviget.minesweeper.game.application;

import com.deviget.minesweeper.dto.Complexity;
import com.deviget.minesweeper.dto.PlayType;
import com.deviget.minesweeper.game.application.mapper.GameDtoMapper;
import com.deviget.minesweeper.game.domain.Game;
import com.deviget.minesweeper.game.repository.GameRepository;
import com.deviget.minesweeper.game.repository.exception.GameNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.*;

class GamesServiceImplUTest {

    @Mock
    private GameRepository gameRepository;

    @Mock
    private GameDtoMapper gameDtoMapper;

    @InjectMocks
    private GamesService gamesService = new GamesServiceImpl();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void createGame() {
        // given
        String playerId = "playerId";
        Complexity complexity = Complexity.LOW;
        when(gameRepository.save(any())).thenReturn(Game.builder()
                .createdBy(playerId)
                .complexity(complexity)
                .build());

        // when
        gamesService.createGame(playerId, complexity);

        // then
        verify(gameRepository, times(1)).save(any());
    }

    @Test
    void readGame_ok() {
        // given
        String playerId = "playerId";
        Complexity complexity = Complexity.LOW;
        Game game = Game.builder()
                .createdBy(playerId)
                .complexity(complexity)
                .build();
        when(gameRepository.findById(any())).thenReturn(Optional.of(game));

        // when
        gamesService.readGame("gameId");

        // then
        verify(gameRepository, times(1)).findById(any());
        verify(gameDtoMapper, times(1)).toGameDto(game);
    }

    @Test
    void readGame_GameNotFoundException() {
        // given
        when(gameRepository.findById(any())).thenReturn(Optional.ofNullable(null));

        // when & then
        assertThrows("GameNotFoundException expected", GameNotFoundException.class, () -> {
            gamesService.readGame("gameId");
        });
    }

    @Test
    void playGame_uncover() {
        // given
        String playerId = "playerId";
        Complexity complexity = Complexity.LOW;
        Game game = Game.builder()
                .createdBy(playerId)
                .complexity(complexity)
                .build();
        when(gameRepository.findById(any())).thenReturn(Optional.of(game));

        // when
        gamesService.playGame("gameId", PlayType.UNCOVER, 0, 0);

        // then
        verify(gameRepository, times(1)).findById(any());
        verify(gameRepository, times(1)).save(any());
        verify(gameDtoMapper, times(1)).toGameDto(any());
    }
}