package com.deviget.minesweeper.game.interfaces;

import com.deviget.minesweeper.common.AbstractApiITest;
import com.deviget.minesweeper.dto.*;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.http.*;

import static org.junit.Assert.*;

class GamesApiITest extends AbstractApiITest {

    @Test
    void test_createGame_success() {
        // given
        String authorizationHeader = login("player1", "player1");
        HttpEntity<CreateGameRequest> request = createGameRequest("player1", Complexity.LOW, authorizationHeader);

        // when
        ResponseEntity postResponse = getRestTemplate().postForEntity(getRootUrl() + "/games", request, String.class);

        // then
        assertNotNull("Response is null", postResponse);
        assertEquals("Status code is not 200", HttpStatus.OK, postResponse.getStatusCode());
        assertNotNull("Response body is null", postResponse.getBody());
        assertTrue("Response body does not contain game id", postResponse.getBody().toString().contains("id"));
    }

    @Test
    void test_createGame_UNAUTHORIZED() {
        // given
        HttpEntity<CreateGameRequest> request = createGameRequest("player1", Complexity.LOW, "");

        // when
        ResponseEntity postResponse = getRestTemplate().postForEntity(getRootUrl() + "/games", request, String.class);

        // then
        assertEquals("Status code is not 401", HttpStatus.UNAUTHORIZED, postResponse.getStatusCode());
    }

    @Test
    void test_createGame_FORBIDDEN() {
        // given
        String authorizationHeader = login("player1", "player1");
        HttpEntity<CreateGameRequest> request = createGameRequest("player2", Complexity.LOW, authorizationHeader);

        // when
        ResponseEntity postResponse = getRestTemplate().postForEntity(getRootUrl() + "/games", request, String.class);

        // then
        assertEquals("Status code is not 403", HttpStatus.FORBIDDEN, postResponse.getStatusCode());
    }

    @Test
    void test_readGame_success() throws JSONException {
        // given
        String authorizationHeader = login("player1", "player1");
        HttpEntity<CreateGameRequest> createGameRequest = createGameRequest("player1", Complexity.LOW, authorizationHeader);
        ResponseEntity postResponse = getRestTemplate().postForEntity(getRootUrl() + "/games", createGameRequest, String.class);
        JSONObject jsonObject = new JSONObject(postResponse.getBody().toString());
        String gameId = jsonObject.getString("id");
        HttpEntity request = emptyRequest(authorizationHeader);

        // when
        ResponseEntity getResponse = getRestTemplate().exchange(getRootUrl() + "/games/{id}", HttpMethod.GET, request, GameWS.class, gameId);

        // then
        assertNotNull("Response is null", getResponse);
        assertEquals("Status code is not 200", HttpStatus.OK, getResponse.getStatusCode());
        assertNotNull("Response body is null", getResponse.getBody());
        assertTrue("Response body does not contain game id", getResponse.getBody().toString().contains("id"));
    }

    @Test
    void test_readGame_NOTFOUND() throws JSONException {
        // given
        String authorizationHeader = login("player1", "player1");
        HttpEntity request = emptyRequest(authorizationHeader);
        String gameId = "inexistent-game";

        // when
        ResponseEntity getResponse = getRestTemplate().exchange(getRootUrl() + "/games/{id}", HttpMethod.GET, request, String.class, gameId);

        // then
        assertNotNull("Response is null", getResponse);
        assertEquals("Status code is not 404", HttpStatus.NOT_FOUND, getResponse.getStatusCode());
    }

    @Test
    void test_playGame_uncover_success() throws JSONException {
        // given
        String authorizationHeader = login("player1", "player1");
        HttpEntity<CreateGameRequest> createGameRequest = createGameRequest("player1", Complexity.LOW, authorizationHeader);
        ResponseEntity createGameResponse = getRestTemplate().postForEntity(getRootUrl() + "/games", createGameRequest, String.class);
        JSONObject jsonObject = new JSONObject(createGameResponse.getBody().toString());
        String gameId = jsonObject.getString("id");
        HttpEntity request = playGameRequest(PlayType.UNCOVER, 0, 0, authorizationHeader); // TODO: Fix flacky test, what if this cell is a mine?

        // when
        ResponseEntity<GameWS> playGameResponse = getRestTemplate().postForEntity(getRootUrl() + "/games/{id}/plays", request, GameWS.class, gameId);

        // then
        assertNotNull("Response is null", playGameResponse);
        assertEquals("Status code is not 200", HttpStatus.OK, playGameResponse.getStatusCode());
        assertNotNull("Response body is null", playGameResponse.getBody());
        assertNotNull("'id' is null", playGameResponse.getBody().getId());
        assertNotNull("'createdBy' is null", playGameResponse.getBody().getCreatedBy());
        assertNotNull("'createdOn' is null", playGameResponse.getBody().getCreatedOn());
        assertNotNull("'status' is null", playGameResponse.getBody().getStatus());
        assertNotNull("'boardConfig' is null", playGameResponse.getBody().getBoardConfig());
        assertEquals(CellStatus.UNCOVERED, playGameResponse.getBody().getCells().get(0).get(0).getStatus());
    }

    @Test
    void test_playGame_redFlag_success() throws JSONException {
        // given
        String authorizationHeader = login("player1", "player1");
        HttpEntity<CreateGameRequest> createGameRequest = createGameRequest("player1", Complexity.LOW, authorizationHeader);
        ResponseEntity createGameResponse = getRestTemplate().postForEntity(getRootUrl() + "/games", createGameRequest, String.class);
        JSONObject jsonObject = new JSONObject(createGameResponse.getBody().toString());
        String gameId = jsonObject.getString("id");
        HttpEntity request = playGameRequest(PlayType.RED_FLAG, 0, 0, authorizationHeader);

        // when
        ResponseEntity<GameWS> playGameResponse = getRestTemplate().postForEntity(getRootUrl() + "/games/{id}/plays", request, GameWS.class, gameId);

        // then
        assertNotNull("Response is null", playGameResponse);
        assertEquals("Status code is not 200", HttpStatus.OK, playGameResponse.getStatusCode());
        assertNotNull("Response body is null", playGameResponse.getBody());
        assertNotNull("'id' is null", playGameResponse.getBody().getId());
        assertNotNull("'createdBy' is null", playGameResponse.getBody().getCreatedBy());
        assertNotNull("'createdOn' is null", playGameResponse.getBody().getCreatedOn());
        assertNotNull("'status' is null", playGameResponse.getBody().getStatus());
        assertNotNull("'boardConfig' is null", playGameResponse.getBody().getBoardConfig());
        assertEquals(CellStatus.RED_FLAG, playGameResponse.getBody().getCells().get(0).get(0).getStatus());
    }

    private HttpEntity<CreateGameRequest> createGameRequest(String playerId, Complexity complexity, String authorizationHeader) {
        CreateGameRequest createGameRequest = new CreateGameRequest();
        createGameRequest.setPlayer(playerId);
        createGameRequest.setComplexity(complexity);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", authorizationHeader);
        HttpEntity<CreateGameRequest> request = new HttpEntity<CreateGameRequest>(createGameRequest, headers);
        return request;
    }

    private HttpEntity<PlayGameRequest> playGameRequest(PlayType playType, int row, int col, String authorizationHeader) {
        PlayGameRequest playGameRequest = new PlayGameRequest();
        playGameRequest.setType(playType);
        playGameRequest.setRow(row);
        playGameRequest.setCol(col);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", authorizationHeader);
        HttpEntity<PlayGameRequest> request = new HttpEntity<PlayGameRequest>(playGameRequest, headers);
        return request;
    }

    private HttpEntity<CreateGameRequest> emptyRequest(String authorizationHeader) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", authorizationHeader);
        HttpEntity request = new HttpEntity(null, headers);
        return request;
    }

}