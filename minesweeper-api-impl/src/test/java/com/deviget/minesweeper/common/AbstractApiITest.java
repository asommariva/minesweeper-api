package com.deviget.minesweeper.common;

import com.deviget.minesweeper.MinesweeperApplication;
import com.deviget.minesweeper.config.security.SecurityConstants;
import com.deviget.minesweeper.dto.LoginRequest;
import lombok.Getter;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MinesweeperApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractApiITest {

    @Autowired
    @Getter
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    protected String getRootUrl() {
        return "http://localhost:" + port;
    }

    protected String login(String username, String password) {
        LoginRequest login = new LoginRequest();
        login.setUsername(username);
        login.setPassword(password);
        ResponseEntity<String> postResponse = getRestTemplate().postForEntity(getRootUrl() + "/login", login, String.class);
        return postResponse.getHeaders().get(SecurityConstants.TOKEN_HEADER).get(0);
    }
}
