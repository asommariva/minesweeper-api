# minesweeper-API

Documentation is structured in the following way:

- [Problem Statement](docs/problem.md): Original readme file with details about the problem to be solved.
- [Solution](docs/solution.md): Decisions taken and important notes.
- [How-to Guides](docs/how-to.md): How to run, test and play the game.
